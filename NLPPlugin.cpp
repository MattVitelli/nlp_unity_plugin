// Example low level rendering Unity plugin
#include "NLPPlugin.h"
#include "Unity/IUnityGraphics.h"

#include <math.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <boost/thread.hpp>
#include <opencv2/opencv.hpp>
#include <nanomsg/nn.h>
#include <nanomsg/pair.h>
#include <libnlp/libnlp.h>

// --------------------------------------------------------------------------
// Include headers for the graphics APIs we support

#if SUPPORT_D3D9
	#include <d3d9.h>
	#include "Unity/IUnityGraphicsD3D9.h"
#endif
#if SUPPORT_D3D11
	#include <d3d11.h>
	#include "Unity/IUnityGraphicsD3D11.h"
#endif
#if SUPPORT_D3D12
	#include <d3d12.h>
	#include "Unity/IUnityGraphicsD3D12.h"
#endif

#if SUPPORT_OPENGLES
	#if UNITY_IPHONE
		#include <OpenGLES/ES2/gl.h>
	#elif UNITY_ANDROID
		#include <GLES2/gl2.h>
	#endif
#elif SUPPORT_OPENGL
	#if UNITY_WIN || UNITY_LINUX
		#include <GL/gl.h>
	#else
		#include <OpenGL/gl.h>
	#endif
#endif

using namespace libnlp;


struct UnityNLPState
{
  int   index;
  bool  isHighlighted;
};

class NLPStateObject
{
public:
  NLPStateObject(std::string url)
  {
    m_url = url;
    m_isConnected = false;
    m_shouldExit = false;
    m_objectListChanged = true;
    m_isStateAvailable = false;
    m_refCount = 0;
    
    boost::thread t0(WriteObjectList, this);
    boost::thread t1(ReadState, this);
  }
  
  ~NLPStateObject()
  {
    for(auto it : m_objectsList)
    {
      delete it.second;
    }
  }
  
  static void WriteObjectList(NLPStateObject *obj)
  {
    int sock = nn_socket (AF_SP, NN_PAIR);
    bool success = (sock >= 0);
    success |= (nn_bind (sock, obj->m_url.c_str()) >= 0);
    assert(success);
    
    obj->m_socket = sock;
    while (!obj->m_shouldExit)
    {
      obj->m_lock.lock();
      if(obj->m_objectListChanged)
      {
        NLPObjectList list;
        for(auto it : obj->m_objectsList)
        {
          NLPObjectDefinition *objDef = list.add_objects();
          *objDef = *it.second;
        }
        std::string msg;
        list.SerializeToString(&msg);
        nn_send(sock, msg.c_str(), msg.size(), 0);
        obj->m_isConnected = true;
        obj->m_objectListChanged = false;
      }
      obj->m_lock.unlock();
    }
    
    obj->m_isConnected = false;
    nn_shutdown(sock, 0);
    nn_close(sock);
  }
  
  static void ReadState(NLPStateObject *obj)
  {
    while (!obj->m_shouldExit)
    {
      if(obj->m_isConnected)
      {
        char *buf = NULL;
        int numBytes = nn_recv (obj->m_socket, &buf, NN_MSG, 0);
        assert (numBytes >= 0);
        
        NLPState state;
        state.ParseFromArray(buf, numBytes);
        obj->m_lock.lock();
        obj->m_state = state;
        obj->m_isStateAvailable = true;
        obj->m_lock.unlock();
        
        nn_freemsg(buf);

      }
    }
    
  }
  
  std::string           m_url;
  int                   m_socket;
  bool                  m_isConnected;
  bool                  m_shouldExit;
  bool                  m_objectListChanged;
  bool                  m_isStateAvailable;
  NLPState                                      m_state;
  boost::mutex                                  m_lock;
  std::map<int, NLPObjectDefinition *>  m_objectsList;
  int                   m_refCount;
};

// --------------------------------------------------------------------------
// Helper utilities


// Prints a string
static void DebugLog (const char* str)
{
	#if UNITY_WIN
	OutputDebugStringA (str);
	#else
	printf ("%s", str);
	#endif
}

// COM-like Release macro
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(a) if (a) { a->Release(); a = NULL; }
#endif


// --------------------------------------------------------------------------
// Global variables
typedef std::shared_ptr<NLPStateObject> NLPStateObjectPtr;
NLPStateObjectPtr  g_nlpState = nullptr;
std::string g_nlp_url = "ipc:///tmp/nlp.ipc";

extern "C" UnityNLPState UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetNLPState()
{
  
  UnityNLPState state;
  state.index = -1;
  state.isHighlighted = false;
  
  if(g_nlpState != nullptr && g_nlpState->m_isStateAvailable)
  {
    state.index = g_nlpState->m_state.object_label();
    state.isHighlighted = g_nlpState->m_state.state_type() == NLPState_NLPStateType_HIGHLIGHT_OBJECT;
  }
  
  return state;
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API Initialize()
{
  if(g_nlpState == nullptr)
  {
    g_nlpState = NLPStateObjectPtr(new NLPStateObject(g_nlp_url));
  }
  
  g_nlpState->m_refCount++;
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetName(int object_index, const char *name)
{
  if(g_nlpState == nullptr)
    return;
  
  g_nlpState->m_lock.lock();
  
  if(g_nlpState->m_objectsList.find(object_index) == g_nlpState->m_objectsList.end())
  {
    NLPObjectDefinition *objDef = new NLPObjectDefinition();
    objDef->set_object_label(object_index);
    g_nlpState->m_objectsList[object_index] = objDef;
  }
  
  g_nlpState->m_objectsList[object_index]->add_object_names(name);
  g_nlpState->m_objectListChanged = true;
  g_nlpState->m_lock.unlock();
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API Shutdown()
{
  if(g_nlpState != nullptr)
  {
    g_nlpState->m_refCount--;
    if(g_nlpState->m_refCount <= 0)
    {
      g_nlpState->m_shouldExit = true;
      g_nlpState = nullptr;
    }
  }
}
